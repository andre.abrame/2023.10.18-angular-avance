import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { SecurityService } from '../services/security.service';
import { map } from 'rxjs';

export const isAdminGuard: CanActivateFn = () => {
  const securityService = inject(SecurityService);
  const router = inject(Router);
  return securityService.authenticatedPerson.pipe(
    map(person => {
      if (!person || person.role !== "ADMIN")
        return router.createUrlTree(["login"]);
      return true;
    })
  )

};
