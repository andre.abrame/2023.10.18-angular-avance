import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SecurityService } from '../services/security.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  constructor(
    private securityService: SecurityService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const user = this.securityService.authenticatedPerson.value;
    if (user) {
      request = request.clone({
        setHeaders: {
          Authorization: "Basic " + btoa(`${user.name}:${user.password}`)
        }
      });
    }
    return next.handle(request);
  }
}
