
export interface Person {
  id?: number;
  name: string;
  password: string;
  dateOfBirth: Date;
  role: "ADMIN" | "USER";
}
