import { createSelector } from "@ngrx/store";
import { Security } from "./security.store";
import { Person } from "../models/person";

export interface AppState {
  security: Security.State
}

export const selectAuthenticatedPerson = (state: AppState) => state.security.authenticatedPerson;

export const selectIsAuthenticated = createSelector(
  selectAuthenticatedPerson,
  (ap: Person | undefined) => ap !== undefined
)

export const selectAuthenticatedPersonRole = createSelector(
  selectAuthenticatedPerson,
  (ap: Person | undefined) => ap?.role
)
