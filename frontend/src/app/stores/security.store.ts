import { createAction, createReducer, on, props } from "@ngrx/store";
import { Person } from "../models/person";
import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { EMPTY, catchError, exhaustMap, map } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";


export namespace Security {

  export interface State {
    authenticatedPerson: Person | undefined
  }

  export const initialState: State = {
    authenticatedPerson: undefined
  }

  export const actions = {
    login: createAction("[Security] login", props<{username: string, password: string}>()),
    loginSucceed: createAction("[Security] login succeed", props<{ person: Person }>()),
    loginFailed: createAction("[Security] login failed"),
    logout: createAction("[Security] logout"),
  }

  export const reducers = createReducer(
    initialState,
    on(actions.loginSucceed, (state, { person }) => ({ ...state, authenticatedPerson: person })),
    on(actions.logout, (state) => ({ ...state, authenticatedPerson: undefined }))
  )

  @Injectable()
  export class Effects {

    login$ = createEffect(() =>
      this.actions$.pipe(
        ofType(actions.login),
        exhaustMap(action => this.httpClient.post<Person>(environment.backendUrl + "/authenticate", {}, { params : {username: action.username, password: action.password} })),
        map(p => actions.loginSucceed({ person: p })),
        catchError(e => EMPTY)
      )
    );

    constructor(
      private actions$: Actions,
      private httpClient: HttpClient
    ) {}

  }


}
