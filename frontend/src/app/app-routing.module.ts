import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonListComponent } from './components/person-list/person-list.component';
import { PersonDetailsComponent } from './components/person-details/person-details.component';
import { PersonNewComponent } from './components/person-new/person-new.component';
import { PersonEditComponent } from './components/person-edit/person-edit.component';
import { LoginComponent } from './components/login/login.component';
import { isAdminGuard } from './guards/is-admin.guard';

const routes: Routes = [
  { path: "", component: PersonListComponent },
  { path: "login", component: LoginComponent },
  { path: "people/new", component: PersonNewComponent, canActivate: [isAdminGuard] },
  { path: "people/:id", component: PersonDetailsComponent },
  { path: "people/:id/edit", component: PersonEditComponent },
  { path: "**", redirectTo: ""}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
