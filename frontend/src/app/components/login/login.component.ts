import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { SecurityService } from 'src/app/services/security.service';
import { AppState } from 'src/app/stores';
import { Security } from 'src/app/stores/security.store';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  username = "";
  password = ""

  constructor(
    // private securityService: SecurityService,
    private store: Store<AppState>,
    private router: Router
  ) {}

  onSubmit() {
    // this.securityService
    //   .login(this.username, this.password)
    //   .subscribe(() => { this.router.navigateByUrl(""); });
    this.store.dispatch(Security.actions.login({username: this.username, password: this.password}));
  }

}
