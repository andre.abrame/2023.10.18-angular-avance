import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Person } from 'src/app/models/person';
import { ValidatorsService } from 'src/app/services/validators.service';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css']
})
export class PersonFormComponent implements OnChanges {

  @Input() editedPerson?: Person;
  @Output() submitPerson = new EventEmitter<Person>();

  personForm = new FormGroup({
    id: new FormControl(0, {nonNullable: true}),
    name: new FormControl('', { asyncValidators: [this.validatorsService.nameAvailable], nonNullable: true }),
    password: new FormControl('', { validators: [Validators.required, Validators.minLength(3)], nonNullable: true}),
    dateOfBirth: new FormControl('', {nonNullable: true}),
    role: new FormControl<"USER" | "ADMIN">('USER', {nonNullable: true})
  })

  constructor(
    private validatorsService: ValidatorsService,
    private router: Router
  ) {}

  ngOnChanges(): void {
      this.personForm = new FormGroup({
        id: new FormControl(this.editedPerson?.id ?? 0, {nonNullable: true}),
        name: new FormControl(this.editedPerson?.name ?? '', { asyncValidators: [this.validatorsService.nameAvailable], nonNullable: true }),
        password: new FormControl(this.editedPerson?.password ?? '', { validators: [Validators.required, Validators.minLength(3)], nonNullable: true}),
        dateOfBirth: new FormControl(this.editedPerson?.dateOfBirth.toISOString().split('T')[0] ?? '', {nonNullable: true}),
        role: new FormControl<"USER" | "ADMIN">(this.editedPerson?.role ?? 'USER', {nonNullable: true})
      });
  }

  onCancel() {
    this.router.navigateByUrl("/");
  }

  onSubmit() {
    if (this.personForm?.invalid) {
      this.personForm?.markAllAsTouched();
    } else {
      this.submitPerson.emit({
          ...this.personForm?.getRawValue(),
          dateOfBirth: new Date(this.personForm?.getRawValue().dateOfBirth)
        });
    }
  }
}
