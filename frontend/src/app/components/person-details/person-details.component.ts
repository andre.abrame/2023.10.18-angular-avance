import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, map, mergeMap } from 'rxjs';
import { Person } from 'src/app/models/person';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css']
})
export class PersonDetailsComponent implements OnInit {

  person$?: Observable<Person>

  constructor(
    private activatedRoute: ActivatedRoute,
    private personService: PersonService
  ) {}

  ngOnInit(): void {
    this.person$ = this.activatedRoute.params.pipe(
      map(params => params["id"]),
      mergeMap(id => this.personService.findById(id))
    );
  }

}
