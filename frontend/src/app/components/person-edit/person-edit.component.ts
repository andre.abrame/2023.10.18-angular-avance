import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, map, mergeMap } from 'rxjs';
import { Person } from 'src/app/models/person';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.css']
})
export class PersonEditComponent implements OnInit {

  person$?: Observable<Person>

  constructor(
    private personService: PersonService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.person$ = this.activatedRoute.params.pipe(
      map(params => params["id"]),
      mergeMap(id => this.personService.findById(id))
    );
  }

  onSubmit(person: Person) {
    this.personService.update(person).subscribe(
      () => this.router.navigateByUrl("/")
    );
  }

}
