import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/models/person';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {

  people?: Person[];

  constructor(
    private personService: PersonService
  ) {}

  ngOnInit(): void {
    this.personService.findAll().subscribe(people => this.people = people);
  }

  onDelete(index: number): void {
    this.personService.deleteById(this.people![index].id!).subscribe({
      next: () => {
        this.people?.splice(index, 1);
      }
    });
  }

}
