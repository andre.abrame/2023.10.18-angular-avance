import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Person } from 'src/app/models/person';
import { SecurityService } from 'src/app/services/security.service';

@Component({
  selector: 'app-person-list-item',
  templateUrl: './person-list-item.component.html',
  styleUrls: ['./person-list-item.component.css']
})
export class PersonListItemComponent {

  @Input() person?: Person;
  @Output() delete: EventEmitter<void> = new EventEmitter<void>();
  authenticatedUser$: Observable<Person | undefined>;

  constructor(
    private router: Router,
    private securityService: SecurityService
  ) {
    this.authenticatedUser$ = securityService.authenticatedPerson;
  }

  onDetails(): void {
    this.router.navigate(["people", this.person?.id]);
  }

  onEdit(): void {
    this.router.navigate(["people", this.person?.id, "edit"]);
  }

  onDelete(): void {
    this.delete.emit();
  }

}
