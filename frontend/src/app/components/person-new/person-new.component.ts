import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Person } from 'src/app/models/person';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-person-new',
  templateUrl: './person-new.component.html',
  styleUrls: ['./person-new.component.css']
})
export class PersonNewComponent {

  constructor(
    private personService: PersonService,
    private router: Router
  ) { }

  onSubmit(person: Person) {
    this.personService.save(person).subscribe(
      () => this.router.navigateByUrl("/")
    );
  }

}
