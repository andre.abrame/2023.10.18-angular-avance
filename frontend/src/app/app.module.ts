import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PersonListComponent } from './components/person-list/person-list.component';
import { PersonDetailsComponent } from './components/person-details/person-details.component';
import { PersonListItemComponent } from './components/person-list-item/person-list-item.component';
import { PersonNewComponent } from './components/person-new/person-new.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrintErrorsDirective } from './shared/print-errors.directive';
import { PersonEditComponent } from './components/person-edit/person-edit.component';
import { PersonFormComponent } from './components/person-form/person-form.component';
import { LoginComponent } from './components/login/login.component';
import { SecurityService } from './services/security.service';
import { AuthenticationInterceptor } from './interceptors/authentication.interceptor';
import { HasRoleDirective } from './directives/has-role.directive';
import { IsAnonymousDirective } from './directives/is-anonymous.directive';
import { IsAuthenticatedDirective } from './directives/is-authenticated.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatButtonModule} from '@angular/material/button';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { Security } from './stores/security.store';

@NgModule({
  declarations: [
    AppComponent,
    PersonListComponent,
    PersonDetailsComponent,
    PersonListItemComponent,
    PersonNewComponent,
    PrintErrorsDirective,
    PersonEditComponent,
    PersonFormComponent,
    LoginComponent,
    HasRoleDirective,
    IsAnonymousDirective,
    IsAuthenticatedDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    StoreModule.forRoot({
      security: Security.reducers
    }, {}),
    EffectsModule.forRoot([
      Security.Effects
    ])
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (ss: SecurityService) => () => ss.init(),
      deps: [SecurityService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      deps: [SecurityService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
