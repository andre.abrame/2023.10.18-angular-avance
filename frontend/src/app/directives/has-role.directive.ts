import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { SecurityService } from '../services/security.service';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, selectAuthenticatedPersonRole } from '../stores';

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective implements OnInit, OnDestroy {

  @Input() appHasRole?: string;
  authUserRole$ = this.store.select(selectAuthenticatedPersonRole);
  private subscription?: Subscription;

  constructor(
    private template: TemplateRef<any>,
    private container: ViewContainerRef,
    // private securityService: SecurityService,
    private store: Store<AppState>
  ) {

  }

  ngOnInit(): void {
      // this.subscription = this.securityService.authenticatedPerson.subscribe(ap => {
      //   if (!ap || ap.role !== this.appHasRole)
      //     this.container.clear();
      //   else
      //     this.container.createEmbeddedView(this.template);
      // })
      this.subscription = this.authUserRole$.subscribe(role => {
        if (!role || role !== this.appHasRole)
          this.container.clear();
        else
          this.container.createEmbeddedView(this.template);
      })
  }

  ngOnDestroy(): void {
      if (this.subscription)
        this.subscription.unsubscribe();
  }

}
