import { Directive, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { SecurityService } from '../services/security.service';

@Directive({
  selector: '[appIsAuthenticated]'
})
export class IsAuthenticatedDirective implements OnInit, OnDestroy {

  private subscription?: Subscription;

  constructor(
    private template: TemplateRef<any>,
    private container: ViewContainerRef,
    private securityService: SecurityService
  ) { }

  ngOnInit(): void {
      this.subscription = this.securityService.authenticatedPerson.subscribe(ap => {
        if (!ap)
          this.container.clear();
        else
          this.container.createEmbeddedView(this.template);
      })
  }

  ngOnDestroy(): void {
      if (this.subscription)
        this.subscription.unsubscribe();
  }

}
