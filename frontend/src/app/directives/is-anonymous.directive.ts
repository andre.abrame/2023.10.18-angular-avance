import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { SecurityService } from '../services/security.service';
import { Store } from '@ngrx/store';
import { AppState, selectAuthenticatedPerson } from '../stores';

@Directive({
  selector: '[appIsAnonymous]'
})
export class IsAnonymousDirective implements OnInit, OnDestroy {

  private subscription?: Subscription;
  private authUser$ = this.store.select(selectAuthenticatedPerson);

  constructor(
    private template: TemplateRef<any>,
    private container: ViewContainerRef,
    // private securityService: SecurityService,
    private store: Store<AppState>
  ) { }

  ngOnInit(): void {
      // this.subscription = this.securityService.authenticatedPerson.subscribe(ap => {
      this.subscription = this.authUser$.subscribe(ap => {
        if (ap)
          this.container.clear();
        else
          this.container.createEmbeddedView(this.template);
      })
  }

  ngOnDestroy(): void {
      if (this.subscription)
        this.subscription.unsubscribe();
  }

}
