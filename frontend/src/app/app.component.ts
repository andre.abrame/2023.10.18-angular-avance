import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SecurityService } from './services/security.service';
import { AppState, selectAuthenticatedPerson } from './stores';
import { Store } from '@ngrx/store';
import { Security } from './stores/security.store';
import { Observable } from 'rxjs';
import { Person } from './models/person';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  authUser$: Observable<Person | undefined>;

  constructor(
    private router: Router,
    // private securityService: SecurityService,
    private store: Store<AppState>
  ) {
    this.authUser$ = store.select(selectAuthenticatedPerson);
  }

  onLogin() {
    this.router.navigateByUrl("login");
  }

  onLogout() {
    this.store.dispatch(Security.actions.logout());
    // this.securityService.logout();
  }
}
