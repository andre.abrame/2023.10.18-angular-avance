import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Person } from '../models/person';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  baseUrl = environment.backendUrl + "/people";

  constructor(private httpClient: HttpClient) {}

  findAll(): Observable<Person[]> {
    return this.httpClient.get<Person[]>(this.baseUrl)
      .pipe(map(people => people.map(p => ({ ...p, dateOfBirth: new Date(p.dateOfBirth) }))));
  }

  findById(id: number): Observable<Person> {
    return this.httpClient.get<Person>(this.baseUrl + "/" + id)
      .pipe(map(p => ({ ...p, dateOfBirth: new Date(p.dateOfBirth) })));
  }

  save(person: Person): Observable<Person> {
    return this.httpClient.post<Person>(this.baseUrl, person)
    .pipe(map(p => ({ ...p, dateOfBirth: new Date(p.dateOfBirth) })));
  }

  update(person: Person): Observable<void> {
    return this.httpClient.put<void>(this.baseUrl + "/" + person.id, person);
  }

  deleteById(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.baseUrl + "/" + id);
  }

  nameAvailable(name: string): Observable<boolean> {
    return this.httpClient.get<boolean>(this.baseUrl + "/nameAvailable/" + name);
  }

}
