import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { Person } from '../models/person';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor(
    private httpClient: HttpClient
  ) { }

  authenticatedPerson: BehaviorSubject<Person | undefined> = new BehaviorSubject<Person | undefined>(undefined);

  init() {
    const person = localStorage.getItem("authenticatedPerson");
    if (person)
      this.authenticatedPerson.next(JSON.parse(person));
  }

  login(username: string, password: string): Observable<Person> {
    return this.httpClient.post<Person>("http://vps-3f8dfeab.vps.ovh.net:8080/authenticate", {}, {
      params : {username: username, password: password}
    }).pipe(
      tap(person => {
        person.password = password;
        this.authenticatedPerson.next(person);
        localStorage.setItem("authenticatedPerson", JSON.stringify(person))
      })
    )
  }

  logout() {
    this.authenticatedPerson.next(undefined);
    localStorage.removeItem("authenticatedPerson");
  }



}
