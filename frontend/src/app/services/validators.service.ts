import { Injectable } from '@angular/core';
import { PersonService } from './person.service';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { map, mergeMap, timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {

  constructor(
    private personService: PersonService
  ) { }

  nameAvailable: AsyncValidatorFn = (control: AbstractControl) => {
    return timer(500).pipe(
      mergeMap(() => this.personService.nameAvailable(control.value)),
      map(available => available ? null : { nameAvailable: { message: "not available"}})
    );
  }

}
