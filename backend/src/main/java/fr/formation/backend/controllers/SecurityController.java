package fr.formation.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.backend.models.Person;
import fr.formation.backend.repositories.PersonRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class SecurityController {

    private PersonRepository personRepository;
    private PasswordEncoder passwordEncoder;
    
    @PostMapping("authenticate")
    public Person authenticate(@RequestParam String username, @RequestParam String password) {
        Person person = personRepository.findByName(username).orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, "invalid username or password"));
        if (!passwordEncoder.matches(password, person.getPassword()))
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "invalid username or password");
        return person;
    }

}
