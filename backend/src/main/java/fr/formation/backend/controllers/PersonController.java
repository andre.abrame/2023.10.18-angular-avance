package fr.formation.backend.controllers;

import java.util.Collection;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.backend.models.Person;
import fr.formation.backend.repositories.PersonRepository;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("people")
@AllArgsConstructor
public class PersonController {
    
    private PersonRepository personRepository;

    private PasswordEncoder passwordEncoder;

	@GetMapping
	public Collection<Person> findAll() {
		return personRepository.findAll();
	}

	@GetMapping("{id:\\d+}")
	public Person findById(@PathVariable long id) {
		return personRepository
            .findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "no user with id " + id + " exists"));
	}
	
	@PostMapping
	public Person save(@Valid @RequestBody Person m) {
		try {
			if (m.getId() != 0)
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "a new entity cannot have a non-null id");
			m.setPassword(passwordEncoder.encode(m.getPassword()));
			return personRepository.save(m);
		} catch (DataIntegrityViolationException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid input");
		}
	}
	
	@PutMapping("{id:\\d+}")
	public Person update(@PathVariable long id, @Valid @RequestBody Person m) {
        if (m.getId() == 0)
            m.setId(id);
		else if (m.getId() != id)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ids in url and body do no match");
		return personRepository.save(m);
	}
	
	@DeleteMapping("{id:\\d+}")
	public void delete(@PathVariable long id) {
		personRepository.deleteById(id);
	}

	@GetMapping("nameAvailable/{name}")
	public boolean nameAvailable(@PathVariable String name) {
		return personRepository.findByName(name).isEmpty();
	}

}
