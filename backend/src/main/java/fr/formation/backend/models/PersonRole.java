package fr.formation.backend.models;

public enum PersonRole {
    USER,
    ADMIN
}
