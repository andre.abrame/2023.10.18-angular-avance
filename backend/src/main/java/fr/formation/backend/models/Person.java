package fr.formation.backend.models;

import java.time.ZonedDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Person {
    
	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private long id;
	
	@NotBlank
    @Column(unique = true)
	private String name;
	
    @NotBlank
	private String password;

	private ZonedDateTime dateOfBirth;

	@Enumerated(EnumType.STRING)
	private PersonRole role;
}
