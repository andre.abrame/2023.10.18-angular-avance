package fr.formation.backend.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(c -> c
                        .requestMatchers(HttpMethod.GET, "people").permitAll()
                        .requestMatchers(HttpMethod.POST, "authenticate").permitAll()
                        .requestMatchers(HttpMethod.POST, "people").hasRole("ADMIN")
                        .requestMatchers(HttpMethod.GET, "people/nameAvailable/{name}").hasRole("ADMIN")
                        .requestMatchers(HttpMethod.DELETE, "people/{id}").hasRole("ADMIN")
                        .requestMatchers(HttpMethod.GET, "people/{id}").permitAll()
                        .requestMatchers("/**").denyAll())
                .formLogin(c -> c.disable())
                .logout(c -> c.disable())
                .csrf(c -> c.disable())
                .sessionManagement(c -> c.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .httpBasic(withDefaults())
                .cors(withDefaults())
                .build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}
