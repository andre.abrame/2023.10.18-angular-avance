package fr.formation.backend.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.backend.models.Person;
import fr.formation.backend.repositories.PersonRepository;
import lombok.AllArgsConstructor;


@Service
@AllArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

	private PersonRepository personRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Person p = personRepository.findByName(username).orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, "name " + username + "not found"));
		return new MyUserDetails(p);
	}

}
