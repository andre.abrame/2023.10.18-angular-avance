package fr.formation.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import fr.formation.backend.models.Person;
import fr.formation.backend.models.PersonRole;
import fr.formation.backend.repositories.PersonRepository;

@SpringBootApplication
public class BackendApplication {

	@Autowired
	private PersonRepository personRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "UPDATE", "DELETE", "OPTIONS")
						.allowedOrigins("*");
			}
		};
	}

	@Bean
	public CommandLineRunner fixtures() {
		return (args) -> {
			if (personRepository.findAll().isEmpty()) {
				personRepository.save(Person.builder()
											.name("a")
											.password(passwordEncoder.encode("a"))
											.role(PersonRole.ADMIN)
											.build());
				personRepository.save(Person.builder()
											.name("u")
											.password(passwordEncoder.encode("u"))
											.role(PersonRole.USER)
											.build());
			}
		};
	}
}
